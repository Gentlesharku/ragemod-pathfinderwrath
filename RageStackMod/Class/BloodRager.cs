﻿using Kingmaker.Blueprints;
using Kingmaker.UnitLogic.Abilities.Blueprints;
using Kingmaker.UnitLogic.Abilities.Components;
using Kingmaker.UnitLogic.Buffs.Blueprints;
using Kingmaker.UnitLogic.Mechanics.Components;
using System.Linq;
using static UnityModManagerNet.UnityModManager;

namespace RageStack
{
    static class BloodRager
    {
        public static ModEntry modEntryAccess;

        public static void initializeFix()
        {

            var demonRageAbility = PatchTools.GetBlueprint<BlueprintAbility>("260daa5144194a8ab5117ff568b680f5");
            var demonRageResource = demonRageAbility.GetComponents<AbilityResourceLogic>().First();

            demonRageAbility.SetComponents(demonRageAbility.ComponentsArray.RemoveFromArray(demonRageResource));

            var DemonRageBuff = PatchTools.GetBlueprint<BlueprintBuff>("36ca5ecd8e755a34f8da6b42ad4c965f");
            var BloodlineRageBuff = PatchTools.GetBlueprint<BlueprintBuff>("5eac31e457999334b98f98b60fc73b2f");

            var DemonRagePowers = DemonRageBuff.GetComponents<AddFactContextActions>().First();
            var BloodlinePowers = BloodlineRageBuff.GetComponents<AddFactContextActions>().First();

            DemonRageBuff.SetComponents(DemonRageBuff.ComponentsArray.RemoveFromArray(DemonRagePowers));

            foreach (var activeAction in BloodlinePowers.Activated.Actions)
            {
                DemonRagePowers.Activated.Actions = DemonRagePowers.Activated.Actions.AppendToArray(activeAction);
            }

            foreach (var deactivateAction in BloodlinePowers.Deactivated.Actions)
            {
                DemonRagePowers.Deactivated.Actions = DemonRagePowers.Deactivated.Actions.AppendToArray(deactivateAction);
            }

            DemonRageBuff.SetComponents(DemonRageBuff.ComponentsArray.AppendToArray(DemonRagePowers));
        }
    }
}
