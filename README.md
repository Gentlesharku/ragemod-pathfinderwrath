# RageMod-PathfinderWrath

With the help of the people and their projects mentioned below, my first mod was to find a solution to make DemonRage and Bloodrager's Rage work together. The solution of this mod is fairly simple add the bloodline traits to DemonRage and make the DemonRage ability's use cap unlimited. That being said, the mod does not look whether or not the character has limitless rage, it just turns off the resource cap for demon rage. 

Do with that as you will, it was made for personal use (Primalist Bloodrager) and seems to be working perfectly for what I wanted for my playthrough. Use and edit as you see fit.

- https://github.com/Vek17/WrathMods-TabletopTweaks
- https://github.com/cabarius/ToyBox/tree/master/ToyBox
- https://www.nexusmods.com/pathfinderwrathoftherighteous/mods/9
- https://github.com/TylerGoeringer/OwlcatModdingWiki/wiki
